import request from '@/utils/request'

// 查询职位管理列表
export function listPost(query) {
  return request({
    url: '/employment/post/list',
    method: 'get',
    params: query
  })
}

// 查询职位管理详细
export function getPost(id) {
  return request({
    url: '/employment/post/' + id,
    method: 'get'
  })
}

// 新增职位管理
export function addPost(data) {
  return request({
    url: '/employment/post',
    method: 'post',
    data: data
  })
}

// 修改职位管理
export function updatePost(data) {
  return request({
    url: '/employment/post',
    method: 'put',
    data: data
  })
}

// 删除职位管理
export function delPost(id) {
  return request({
    url: '/employment/post/' + id,
    method: 'delete'
  })
}
