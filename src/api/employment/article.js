import request from '@/utils/request'



// 查询资讯分类名称
export function channelList(query) {
  return request({
    url: '/employment/channel/list',
    method: 'get',
    params: query
  })
}

// 查询资讯分类列表
export function listArticle(query) {
  return request({
    url: '/employment/article/list',
    method: 'get',
    params: query
  })
}

// 查询资讯分类详细
export function getArticle(id) {
  return request({
    url: '/employment/article/' + id,
    method: 'get'
  })
}

// 新增资讯分类
export function addArticle(data) {
  return request({
    url: '/employment/article',
    method: 'post',
    data: data
  })
}

// 修改资讯分类
export function updateArticle(data) {
  return request({
    url: '/employment/article',
    method: 'put',
    data: data
  })
}

// 删除资讯分类
export function delArticle(id) {
  return request({
    url: '/employment/article/' + id,
    method: 'delete'
  })
}
