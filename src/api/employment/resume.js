import request from '@/utils/request'

// 查询简历管理列表
export function listResume(query) {
  return request({
    url: '/employment/resume/list',
    method: 'get',
    params: query
  })
}

// 查询简历管理详细
export function getResume(id) {
  return request({
    url: '/employment/resume/' + id,
    method: 'get'
  })
}

// 预览简历详细
export function resumeInfo(studentId,resumeId) {
  return request({
    url: '/employment/resume/resumeinfo/' + studentId + "/" + resumeId,
    method: 'get'
  })
}

// 新增简历管理
export function addResume(data) {
  return request({
    url: '/employment/resume',
    method: 'post',
    data: data
  })
}

// 修改简历管理
export function updateResume(data) {
  return request({
    url: '/employment/resume',
    method: 'put',
    data: data
  })
}

// 删除简历管理
export function delResume(id) {
  return request({
    url: '/employment/resume/' + id,
    method: 'delete'
  })
}


// 检查状态
export function checkStatus() {
  return request({
    url: '/employment/resume/checkStatus' ,
    method: 'get'
  })
}


//获取用户对象

export function getById() {
  return request({
    url: '/employment/resume/getUserById/',
    method: 'get'
  })
}
