import request from '@/utils/request'
import {parseStrEmpty} from "../../utils/ruoyi";

// 查询社交列表
export function listWebinfo(query) {
  return request({
    url: '/employment/webinfo/list',
    method: 'get',
    params: query
  })
}

// 查询社交详细
export function getWebinfo(id) {
  return request({
    url: '/employment/webinfo/' + parseStrEmpty(id),
    method: 'get',
  })
}

// 新增社交
export function addWebinfo(data) {
  return request({
    url: '/employment/webinfo',
    method: 'post',
    data: data
  })
}

// 修改社交
export function updateWebinfo(data) {
  return request({
    url: '/employment/webinfo',
    method: 'put',
    data: data
  })
}

// 删除社交
export function delWebinfo(id) {
  return request({
    url: '/employment/webinfo/' + id,
    method: 'delete'
  })
}
