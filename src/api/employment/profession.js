import request from '@/utils/request'

// 查询行业管理列表
export function listProfession(query) {
  return request({
    url: '/employment/profession/list',
    method: 'get',
    params: query
  })
}

// 查询行业管理详细
export function getProfession(id) {
  return request({
    url: '/employment/profession/' + id,
    method: 'get'
  })
}

// 新增行业管理
export function addProfession(data) {
  return request({
    url: '/employment/profession',
    method: 'post',
    data: data
  })
}

// 修改行业管理
export function updateProfession(data) {
  return request({
    url: '/employment/profession',
    method: 'put',
    data: data
  })
}

// 删除行业管理
export function delProfession(id) {
  return request({
    url: '/employment/profession/' + id,
    method: 'delete'
  })
}
