import request from '@/utils/request'

// 查询资讯分类列表
export function listChannel(query) {
  return request({
    url: '/employment/channel/list',
    method: 'get',
    params: query
  })
}

// 查询资讯分类详细
export function getChannel(id) {
  return request({
    url: '/employment/channel/' + id,
    method: 'get'
  })
}

// 新增资讯分类
export function addChannel(data) {
  return request({
    url: '/employment/channel',
    method: 'post',
    data: data
  })
}

// 修改资讯分类
export function updateChannel(data) {
  return request({
    url: '/employment/channel',
    method: 'put',
    data: data
  })
}

// 删除资讯分类
export function delChannel(id) {
  return request({
    url: '/employment/channel/' + id,
    method: 'delete'
  })
}
