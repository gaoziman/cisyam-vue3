import request from '@/utils/request'

// 查询实习经历管理列表
export function listExperience(query) {
  return request({
    url: '/employment/experience/list',
    method: 'get',
    params: query
  })
}

// 查询实习经历管理详细
export function getExperience(id) {
  return request({
    url: '/employment/experience/' + id,
    method: 'get'
  })
}

// 新增实习经历管理
export function addExperience(data) {
  return request({
    url: '/employment/experience',
    method: 'post',
    data: data
  })
}

// 修改实习经历管理
export function updateExperience(data) {
  return request({
    url: '/employment/experience',
    method: 'put',
    data: data
  })
}

// 删除实习经历管理
export function delExperience(id) {
  return request({
    url: '/employment/experience/' + id,
    method: 'delete'
  })
}
