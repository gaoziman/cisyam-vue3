import request from '@/utils/request'

// 查询职位管理列表
export function listTpost(query) {
    return request({
        url: '/employment/tpost/list',
        method: 'get',
        params: query
    })
}

// 查询职位管理详细
export function getTpost(id) {
    return request({
        url: '/employment/tpost/' + id,
        method: 'get'
    })
}

// 新增职位管理
export function addTpost(data) {
    return request({
        url: '/employment/tpost',
        method: 'post',
        data: data
    })
}

// 修改职位管理
export function updateTpost(data) {
    return request({
        url: '/employment/tpost',
        method: 'put',
        data: data
    })
}

// 删除职位管理
export function delTpost(id) {
    return request({
        url: '/employment/tpost/' + id,
        method: 'delete'
    })
}
